if(document.getElementById("getElemButton") != null){
document.getElementById("getElemButton").addEventListener("click", getElem);
}
if(document.getElementById("btnLanguage") != null){
    document.getElementById("btnLanguage").addEventListener("click", getLanguage);
}
if(document.getElementById("btnEmails") != null){
    document.getElementById("btnEmails").addEventListener("click", getEmails);
}
if(document.getElementById("btnData") != null){
    document.getElementById("btnData").addEventListener("click", calculateData);
}
if(document.getElementById("GenerateTable") != null){
    document.getElementById("GenerateTable").addEventListener("click", GenerateTable)
}
function getElem(){
let elem = document.getElementById("footer");
let str = document.getElementById("header").innerText;
elem.innerText = "© " + str;
}

function getLanguage(){
let elements = document.getElementsByName("lang");
let str =  "";
for(let i = 0; i<elements.length; i++){
    if(elements[i].checked){
        str += elements[i].getAttribute("id") + " ";
    }
}
    document.getElementById("outputLanguage").innerText = str;
}

function getEmails(){
    let elements = document.getElementsByName("email");
    let str = "";
    for(let i = 0; i < elements.length; i++){
        if(elements[i].checked){
            str += elements[i].getAttribute("id") + "@gmail.com; ";
        }
    }
    document.getElementById("outputEmails").innerText = str.slice(0, str.length - 2);
}

function calculateData(){
    let elements = document.getElementsByName("monthInput");
    let sum = 0, min = parseInt(elements[0].value), max = parseInt(elements[0].value);
    for(let i = 0; i < elements.length; i++){
        sum += parseInt(elements[i].value);
        if(parseInt(elements[i].value) > max){
            max = parseInt(elements[i].value);
        }
        if(parseInt(elements[i].value) < min){
            min = parseInt(elements[i].value);
        }
    }
    let average = Math.round(sum/12);
    document.getElementById("dataAverage").innerText = average + ";";
    document.getElementById("dataSum").innerText = sum + ";";
    document.getElementById("dataMin").innerText = min + ";";
    document.getElementById("dataMax").innerText = max + ";";
}

function GenerateTable(){
    if(document.getElementById("readyTable") != null)
    {
        document.body.removeChild(document.getElementById("readyTable"));
    }
    let table = document.createElement("table");
    table.className = "tableColors";
    table.id = "readyTable";
    let row;
    let cell;
    let n = parseInt(document.getElementById("inputN").value);
    let m = parseInt(document.getElementById("inputM").value);
    document.body.appendChild(table);
    for(let i = 0; i < n; i++){
        row = document.createElement("tr");
        row.id = "row" + i;
        table.appendChild(row);
        for(let j = 0; j < m; j++){
            cell = document.createElement("td");
            document.getElementById("row" + i).appendChild(cell);
            cell.id = "cell" + j
            cell.className = "available";
        }
    }
    table.addEventListener("click", function switchCondition(event){
        if(event.target.className == "available")
        {
            event.target.className = "not-available";
        }
        else
        if(event.target.className == "not-available")
            event.target.className = "available";
    });
}
if (document.getElementById("runningButton") != null){
    let left, top;
    let button = document.getElementById("runningButton");
    let maxLeft = document.documentElement.clientWidth-button.offsetWidth;
    let maxTop = document.documentElement.clientHeight-button.offsetHeight;
    button.addEventListener("mouseenter", function Run()
    {
        left = Math.random() * maxLeft;
        top = Math.random() * maxTop;
        button.style.left = left + "px";
        button.style.top = top + "px";
    });
}

if(document.getElementById("Guy") != null){
    let guy = document.getElementById("Guy");
    let x, y;
    guy.addEventListener("dragstart", function (event){
        x = event.offsetX;
        y = event.offsetY;
    });
    guy.addEventListener("dragend", function (event){
        guy.style.top = (event.pageY - y) + "px";
        guy.style.left = (event.pageX - x) + "px";
    });
}